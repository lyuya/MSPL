---
title: "Projet Statistique:The analysis of the data set about the quantity of the medical fonctions in the commune or district around Paris"
author: "Yanan LYU / Jefferson Cessna "
date: "04/04/2018"
output:
  pdf_document: 
    toc: yes
  html_document: default
---
\newpage



\newpage

#Description of the data:

###column:33
###row:1300
###content de comlumn \

 Département:The number of the french departement that each city belongs to \
 Département commune:The numbre of the departement commune that each city belongs to \
 Libellé_de_commune:The name of each city \
 Arrondissement:The number of the district that the cities belongs to \
 Canton_ville:The Pseudo-canton that each city belongs to \
 Zone d'emploi:The work zone that each city belongs to \
 Unité urbaine:The statistical area defined by INSEE \
 Population_2010:The population of each city in 2010 \
 Médecin_omnipraticien:The amount of this medical fonction in each city \
 Spécialiste_en_cardiologie:The amount of this medical fonction in each city \
 Spécialiste_en_dermatologie_vénéréologie:The amount of this medical fonction in each city \
 Spécialiste_en_gynécologie_médicale:The amount of this medical fonction in each city \
 Spécialiste_en_gynécologie_obstétrique:The amount of this medical fonction in each city \
 Spécialiste_en_gastro-entérologie_hépatologie:The amount of this medical fonction in each city \
 Spécialiste_en_psychiatrie:The amount of this medical fonction in each city \
 Spécialiste_en_ophtalmologie:The amount of this medical fonction in each city \
 Spécialiste_en_oto-rhino-laryngologie:The amount of this medical fonction in each city \
 Spécialiste_en_pédiatrie:The amount of this medical fonction in each city \
 Spécialiste_en_pneumologie:The amount of this medical fonction in each city \
 Spécialiste_en_radiodiagnostic_et_imagerie_médicale:The amount of this medical fonction in each city \
 Spécialiste_en_stomatologie:The amount of this medical fonction in each city \
 Chirurgien_dentiste:The amount of this medical fonction in each city \
 Sage-femme:The amount of this medical fonction in each city \
 Infirmier:The amount of this medical fonction in each city \
 Masseur_kinésithérapeute:The amount of this medical fonction in each city \
 Orthophoniste:The amount of this medical fonction in each city \
 Orthoptiste:The amount of this medical fonction in each city \
 Pédicure-podologue:The amount of this medical fonction in each city \
 Audio_prothésiste:The amount of this medical fonction in each city \
 Ergothérapeute:The amount of this medical fonction in each city \
 Psychomotricien:The amount of this medical fonction in each city \
 Lon:The longitude of the position of each city \
 Lat:The latitude of the position of each city \
 

#Introduction

Tha data describe the distribution of the medical functions by departements, work zones, and it also provide the data of the population of each departement,so we can find the relation between the quantity of the function and the population of the unit geographic,or the position of the unit geographic.

##The source of the data:

https://www.data.gouv.fr/fr/datasets/nombre-de-fonctions-medicales-et-paramedicales-par-commune-ou-arrondissement-base-permanente-des-equipements/#_

##The question and the description:

1.Which geographic feature of the distribution of the medical fonction in the commune or district around Paris and how? \
2.How is the distribution of the different type of the medical fonction? \

For the first question,we arrange the data with different geographic symbol and try to summarize the feature of the distribution. \
For second question, we analysis the data in aspect of the relation between the population and the amount of the medical fonctions in Paris region,and we try to research the relation lineaire. 


#Methodology



##Data clean-up procedures:

For cleaning the data,first we deal with "Numbers" in mac .We seperate the data with "," and ";",so that each column has only one variable.Also we delated a column that we will not use for the analysis of the data.

##Data representation choices:

We choose to introduce our analysis by pieplot,barplot,boxplot,jitterplot,scatterplot,.



#Analysis in Literate Programming

###Question 1:

```{r}
library(ggplot2);
library(dplyr);
library(sp);
library(magrittr);
library(rgeos);
library(rworldmap);
library(ggmap);
library(plotrix);
library(plotly);
df <- read.csv("medicale.csv", 
               header=TRUE, sep=";", dec=",")


departement = df$Département 
arrondissement = df$Arrondissement 
lon = df$Lon
lat = df$Lat
omnipraticien<-sum(df$Médecin_omnipraticien)
cardiologie<-sum(df$Spécialiste_en_cardiologie)
dermatologie<-sum(df$Spécialiste_en_dermatologie_vénéréologie)
gynécologie<-sum(df$Spécialiste_en_gynécologie_médicale)
gynécologie_obstétrique<-sum(df$Spécialiste_en_gynécologie_obstétrique)
gastro<-sum(df$Spécialiste_en_gastro.entérologie_hépatologie)
psychiatrie<-sum(df$Spécialiste_en_psychiatrie)
ophtalmologie<-sum(df$Spécialiste_en_ophtalmologie)
rhino<-sum(df$Spécialiste_en_oto.rhino.laryngologie)
pédiatrie<-sum(df$Spécialiste_en_pédiatrie)
pneumologie<-sum(df$Spécialiste_en_pneumologie)
radiodiagnostic<-sum(df$Spécialiste_en_radiodiagnostic_et_imagerie_médicale)
stomatologie<-sum(df$Spécialiste_en_stomatologie)
dentiste<-sum(df$Chirurgien_dentiste)
sage<-sum(df$Sage.femme)
Infirmier<-sum(df$Infirmier)
kine<-sum(df$Masseur_kinésithérapeute)
orthophoniste<-sum(df$Orthophoniste)
Orthoptiste<-sum(df$Orthoptiste)
pedicure<-sum(df$Pédicure.podologue)
prothésiste<-sum(df$Audio_prothésiste)
ergothérapeute<-sum(df$Ergothérapeute)
psychomotricien<-sum(df$Psychomotricien)

```

From the histogram of the  "Médecin_omnipraticien",apparently we can tell the numbre of the general doctor in each city is basicly between 0 and 50.

```{r}
color = rainbow(4)
h=hist(df$Médecin_omnipraticien, probability = TRUE, col=color)

```

We aggregate the data of the population of the region and the quantity of the general doctor,so that we can find the relation between the population and the quantity of the general doctor.

```{r}
a = aggregate(df$Population_2010, by=list(departement), FUN=sum)
b = aggregate(df$Médecin_omnipraticien, by=list(departement), FUN=sum)

#a%>%head(n=5)
#b%>%head(n=5)
```

This barplot implique that in each department,in average,each general doctor can serve how many peole.From the graph we can tell the quality of service in 75 departement is more convenient and more reachable for the citizens. \

```{r}
a1 <- data.frame(round(a$x / b$x, 2)) 
#a1
pourcentPopu<-data.frame(as.character(c("75","77","78","91","92","93","94","95")) ,a1$round.a.x.b.x..2.)
#pourcentPopu
g <- ggplot(pourcentPopu, aes(x = a$Group.1,y = a1$round.a.x.b.x..2.))
distribuPourcent<- g + geom_bar(stat="identity")
distribuPourcent
```


We arrange the data of the population and from the pie graph we can know the distribution of the population by departments. \
```{r}
pieval<-a$x
 pielabels<-a$Group.1
   myLabel = paste(pielabels,"(", round(a$x / sum(a$x) * 100, 2), "%)", sep = "")
 # grab the radial positions of the labels
lp<-pie(pieval,radius=0.8,labels=myLabel,main="The population of the departements",col=c("brown","#ddaa00","pink","#dd00dd","yellow","red","purple","violet"))

```


For find the distribution of the quantity of all the in different departement,It should create a variable to collect the result of the addition of all the  medical fonction in each departement. \
```{r}
medecin = df$Médecin_omnipraticien+df$Spécialiste_en_cardiologie+df$Spécialiste_en_dermatologie_vénéréologie+df$Spécialiste_en_gynécologie_médicale+df$Spécialiste_en_gynécologie_obstétrique+df$Spécialiste_en_gastro.entérologie_hépatologie+df$Spécialiste_en_psychiatrie+df$Spécialiste_en_ophtalmologie+df$Spécialiste_en_oto.rhino.laryngologie+df$Spécialiste_en_pédiatrie+df$Spécialiste_en_pneumologie+df$Spécialiste_en_radiodiagnostic_et_imagerie_médicale+df$Spécialiste_en_stomatologie+df$Chirurgien_dentiste+df$Sage.femme+df$Infirmier+df$Masseur_kinésithérapeute+df$Orthophoniste+df$Orthoptiste+df$Pédicure.podologue+df$Audio_prothésiste+df$Ergothérapeute+df$Psychomotricien

sommeMed<-data.frame(df$Département,medecin)
```

Then we calcule the total amount of the medical fonction by work zone in France. \

```{r}
distribZoneDemploi<-data.frame(df$Zone.d.emploi,medecin)
distrZagg<-aggregate(medecin,by=list(df$Zone.d.emploi),FUN = sum)
```

Then we analyse the distribution of the quantity of the medical funtion by work zone, we can get conclusion that the work zone "1101" occupe the biggest part. \
```{r,fig.height=5,fig.width=10}
barplot(distrZagg$x,names.arg = distrZagg$Group.1,xlab = "zone d'emploi",ylab = "",col = "grey", main = "La distribution de nombre total de fonction médicale et paramédicale en zone d'emploi", las = 2)
```

We calcule the sum of all the medical fonction by each departement.And the barplot explicitly describe the distribution. \

```{r}
aSommeMed = aggregate(medecin, by=list(departement), FUN=sum)

barplot(aSommeMed$x,names.arg = aSommeMed$Group.1,xlab = "Département",ylab = "La somme des médecins",col = "grey",main = "")
```

Here the point graph,and it illustrate that in each city, the quantity of the general doctor has positive correlation with the population. \

```{r}
ggplot(data = df) +
  geom_point(mapping = aes(x = df$Médecin_omnipraticien, y = df$Population_2010)) +
  geom_smooth(mapping = aes(x = df$Médecin_omnipraticien, y = df$Population_2010))
```
We calcule the sum of the amount of medical fonction by district of Paris and the sum of the population of each district. \
In this  jitter geom,we can also tell that there's the positive correlation between the population of and the quantity of the medical fonctions.That's to say the bigger the population is the bigger the quantity of the medical function. \
```{r}
c = aggregate(list(medecin,df$Population_2010) ,by= list(df$Arrondissement),FUN=sum)
nbMedArr<-c$c.0L..0L..21L..0L..0L..10L..1L..0L..0L..1L..4L..27L..0L..6L..
populArr<-c$c.268L..624L..9626L..1180L..414L..1574L..242L..574L..164L..1142L..

ggplot(c, aes(nbMedArr,populArr)) + geom_jitter()
```

#Question 2 \


We calcule the sum of each type of the medical fonction. \
In order to find out the feature of the distribution of the different types of the medical fontions.We convert the name of the column as the data in a column as sum of the each type of the medical fonction. \

```{r}
medecins <- data.frame(omnipraticien,cardiologie,dermatologie,gynécologie,gynécologie_obstétrique,gastro,psychiatrie,ophtalmologie,rhino,pédiatrie,pneumologie,radiodiagnostic,stomatologie,dentiste,sage,Infirmier,kine,orthophoniste,Orthoptiste,pedicure,prothésiste,ergothérapeute,psychomotricien)
mz <- data.frame(t(medecins)) 
#convert the direction of the matrix
mz%>%head(n=5)
```

This barplot explique the distribution of different types of the medical fonctions. \

```{r}
dfbar<- data.frame(
  sommM=c("Médecin_omnipraticien","Spécialiste_en_cardiologie","Spécialiste_en_dermatologie_vénéréologie","Spécialiste_en_gynécologie_médicale","Spécialiste_en_gynécologie_obstétrique","Spécialiste_en_gastro-entérologie_hépatologie","Spécialiste_en_psychiatrie","Spécialiste_en_ophtalmologie","Spécialiste_en_oto-rhino-laryngologie","pédiatrie","Spécialiste_en_pneumologie","radiodiagnostic_et_imagerie_médicale","Spécialiste_en_stomatologie","	Chirurgien_dentiste","Sage-femme","Infirmier","Masseur_kinésithérapeute","Orthophoniste","	Orthoptiste","Pédicure-podologue","Audio_prothésiste","Ergothérapeute","Psychomotricien"),
+mz$t.medecins.)
barplot(mz$t.medecins.,
        names.arg = dfbar$sommM,
        xlab = "",
        ylab = "La somme des médecins",
        col = "grey", 
        main = "", 
        las = 2)
```

From this boxplot we can see that the majority of the amount of the medical fonction is around less than 2000. \

```{r}
boxplot(dfbar$X.mz.t.medecins.)
```




#Conclusion

###Question 1: \

In comparaision vertical,the distribution of the medical fonction in the region more central(closer to the city center) is apparently more wide,but in the same time the population is bigger,and the average amount of the citizens served for each medical fonction is the lowest.That's to say,the medical level in this region is better.And for the citizens,it's more convenients in daily life.

###Question 2: \

In all type of medical fonction,the médecin omnipraticien(general doctor) take part in the biggest part,kinésithérapeute(physiotherapist) is the second,and the dentist is the third.

#References

1.http://ggplot2.tidyverse.org/index.html \
2.https://www.insee.fr/fr/metadonnees/definition/c1361 \
3.https://www.invensis.net/blog/data-processing/5-steps-data-cleansing-customer-data/ \
4.https://www.r-bloggers.com/box-plot-with-r-tutorial/ \
5.https://stackoverflow.com/questions/20442693/how-to-use-ggplot2-to-generate-a-pie-graph \
6.https://github.com/dnafrance/r-markdown-cheatsheet/blob/master/r-markdown-cheatsheet.rmd \
7.https://blog.csdn.net/qq_35242986/article/details/69503875 \






